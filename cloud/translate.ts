// % ! (verbose)(defaultTerm)
// % #defaultTerm env set {loggerTerminalName: "myLogger", outputTerminalName:"myOutput"}
// % #verbose env set {verbose: true}

let sys;


let fetch = require('node-fetch')



// % fetchTranslate (getCredentials) "Ribakood"
let fetchTranslate = async function(apiKey:string, text:string, source:string="et", dest:string="en", callback:Function) {

    //sys.logger.info("Got api key: " + apiKey);

    let url = `https://www.googleapis.com/language/translate/v2?key=${apiKey}&source=${source}&target=${dest}&q=${text}`;
    //sys.logger.info("URL " + url);

    let result = await fetch(url,
    {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    });
    let data = await result.json();
    if (data.error) {
        sys.logger.error(data.error.message);
    }

    if (data.data && data.data.translations.length) {
        let word = data.data.translations[0].translatedText;
        if (callback) {
            callback(word);
        }
        return word;
    }

}


exports = {fetchTranslate}




// % sys.pmem

// % tryTranslate "AIzaSyAdGh6dWqp2tFs1WMt6smfS4FHY3f9p_y4" "Ribakood"
let tryTranslate = function(apiKey, text:string, source:string, dest:string) {
    if (!source) {
        source = "et";
    }
    if (!dest) {
        dest = "en";
    }

    let translate = sys.pmem.translate;

    if (!translate) {
        translate = sys.pmem.translate = {};
    }

    let sourceLang = translate[source];
    if (!sourceLang) {
        sourceLang = translate[source] = {};
    }

    let destLang = sourceLang[dest];
    if (!destLang) {
        destLang = sourceLang[dest] = {};
    }

    let word = destLang[text];
    if (word) {
        // found translation
        return word;
    }

    // no translation found; fire off a request to Google
    fetchTranslate(apiKey, text, source, dest, function(transText) {
        // got a Google response !!
        sys.logger.info("Got translation of word [" + text + "] = " + transText);
        destLang[text] = transText;
        debouncedSave();
    });

    // will return undefined if not found
}

// % debouncedSave
let debouncedSave = sys.util.debounce(async function() {
    sys.logger.info("Saving");
    await sys.saveMemory();
},1000);


let translateData = function(apiKey:string, data:Array<Object>, source:string,dest:string) {

    if (!data) {
        throw new Error("No data provided");
    }

    let finalResult = [];
    data.map(function(priorRow) {
        let outRow = {};

        Object.keys(priorRow).map(function(colName) {
            let colValue = priorRow[colName];
            outRow[colName] = colValue;

            let colType = sys.util.valueType(colValue);
            if (colType === "string") {
                let transText = tryTranslate(apiKey, colValue, source, dest);
                if (transText) {
                    outRow[colName + "_translate"] = transText;
                } else {
                    outRow[colName + "_translate"] = "-";
                }
            }
        });

        finalResult.push(outRow);
    });

    return finalResult;

}


let translateHeader = function(apiKey:string, data:Array<Object>, source:string,dest:string) {

    let outData = [];

    if (data.length) {
        let firstRow = data[0];

        let colMap = {};
        let revColMap = {};

        Object.keys(firstRow).map(function(colName, idx) {
            sys.logger.info("Processing column [" + colName + "]");
            let transColName = tryTranslate(apiKey, colName, source, dest);
            if (transColName) {
                if (Reflect.has(revColMap, transColName)) {
                    sys.logger.warn("Column [" + transColName + "] appears more than once");
                    transColName += idx;
                }
                colMap[colName] = transColName;
                revColMap[transColName] = colName;
            } else {

                colMap[colName] = colName;
            }
        });

        //sys.logger.inspect(colMap)
        //sys.logger.inspect(revColMap)

        data.map(function(row) {
            //sys.logger.inspect(row);
            let newRow = {};
            Object.keys(row).map(function(rk) {
                let newName = colMap[rk];
                if (newName) {
                    newRow[newName] = row[rk];
                } else {
                    sys.logger.error("Unable to map column [" + rk + "]")
                }
            });
            //sys.logger.inspect(newRow);
            outData.push(newRow);
        });
    }

    return outData;

}



