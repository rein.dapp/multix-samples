// % ! (verbose)(defaultTerm)
// % #defaultTerm env set {loggerTerminalName: "myLogger", outputTerminalName:"myOutput"}
// % #verbose env set {verbose: true}

// % multix import translate

// @ts-ignore
let sys;

// % help target

// % date

// % sh hostname
// % sh cd /tmp/foo
// % sh ls -la
// % sh open Receipt.xlsx


// % sh touch credentials.json
// % sh source ~/.zshrc
// % sh edit credentials.json


let myArray = [
    {id:5,name:"bart"},
    {id:7,name:"maggie"}
]

let getmyarray = function() {
    return myArray;
}

// % select * from shell("myArray")
// % select * from getmyarray()

// % select * from xlsx("/tmp/foo/Receipt.xlsx") order by Summa



// % fetchTranslate "AIzaSyAdGh6dWqp2tFs1WMt6smfS4FHY3f9p_y4" "Ribakood"

// % exec translate (fetchTranslate "AIzaSyAdGh6dWqp2tFs1WMt6smfS4FHY3f9p_y4" "Ribakood")


// % select * from xlsx("/tmp/foo/Receipt.xlsx") >> doTranslate
let doTranslate = async function() {
    let params = {
        apiKey: getCredentials(),
        data: sys.prior,
        source:"et",
        dest:"en"
    };

    let result;
    result = await sys.exec(null, "translate", "translateData", params);

    params.data = result;

    result = await sys.exec(null, "translate", "translateHeader", params);

    return result;
}



// % #myTable (select * from xlsx("/tmp/foo/Receipt.xlsx") >> doTranslate)
// % myTable

// % select * REMOVE [Name of Product] from shell("myTable") order by [Sum]



let fs = require("fs");

// % getCredentials
let getCredentials = function():string {
    //return require
    //delete require.cache[require.resolve("/tmp/foo/config.json")]
    let credsFile = fs.readFileSync("/tmp/foo/credentials.json",'utf8');
    let creds = JSON.parse(credsFile);
    return creds.APIKEY;
}


exports = {getCredentials}